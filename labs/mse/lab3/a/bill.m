#import <Foundation/Foundation.h>
#import "item.h"
#import "bill.h"

@implementation Bill 
- (id) initWithItem:(NSObject<Item>*)i {
	item = i;
	taxObject = [[[i getTaxType] alloc] init];

	return self;
}

- (double) getPrice {
	return [item getPricePerUnit] * [item getQuantity];
}

- (double) getTax {
	return [taxObject getTax:[self getPrice]];
}

- (double) getTotalPrice {
	return [taxObject getTotalPrice:[self getPrice]];
}
@end
