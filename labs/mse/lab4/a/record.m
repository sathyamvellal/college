#import <Foundation/Foundation.h>
#import "record.h"

@implementation Record 

@synthesize name;
@synthesize email;
@synthesize phoneNumber;

- (id) initWithName:(NSString*)paramName initWithEmail:(NSString*)paramEmail initWithPhoneNumber:(NSString*)paramPhoneNumber {
	name = paramName;
	email = paramEmail;
	phoneNumber = paramPhoneNumber;

	return self;
}

- (NSString*)description {
	return [NSString stringWithFormat:@"Name: %@, Email: %@, PhoneNumber: %@", name, email, phoneNumber];
}
@end
