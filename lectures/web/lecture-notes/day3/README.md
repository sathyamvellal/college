13-08-2013
=========

### REST
**RE**presetnational **S**tate **T**ransfer  
Five criteria for a system to be restful :

1. Client Server based
2. The system should be stateless
    * The server won't have any previous history of requests
3. Response from the server must be **cache-able** on the client.
    * Helps in saving a lot of bandwidth
    * Some data which doesn't change frequently can be saved
4. Architecture should be **layered**
    * Maybe the server is made up of a cluster of servers but the client does not know this.   
    * There is an interface to the client and the entire cluster is the server.
    * For obvious reasons on the server side this is very helpful
5. Client and Server have a uniform interface.
    * They evolve independently 
    * The interface does not change.
    * Maybe implementation/features change over time.
    
Is the Web REST-ful? 

* Yes
* Yes
* The caching is handled through the Header field in the response. There is a field called **cache-control**. ```cache-control:  no-cache``` indicates no caching. 
* Yes
* Yes

---
#### How HTML5 started

* 2008
* Started with the browsers. Browsers were not following the XHTML standard uniformly.

---
#### Important things in HTML5

* input elements
* canvas
* web workers
* local storage
* offline browsing
* semantic elements

#### Four ways to figure out if an element is present or not in HTML5

* check for property on ```window```. May not be part of ```window```, so not a good idea.
* Create an element. Check for a property (some default properties of that element) ```//smart!```
* Create an element and check for a method. For example with ```canvas```, we can do something like this 
```
var can = document.createElement ("canvas");
if (!!can.getContext()) {
.
.
}
```
* Use a javascript library - ```modernizer.js```

Browser will know if its a HTML5 page by ```<!DOCTYPE html>```

---
#### HTML5 video

* src
* controls
* preload
    * none
    
---
#### Semantic tags/elements

* No effect on format
* Main intention is for the reader of the source.
* Tags
    * article
        * Would define the entire article.
        * More than one article, have more tags
    * section
        * Part of an article
    * nav
        * links go here.
    * hgroup
        * group header elements
    * footer
        * you know that this is
    * aside
        * Example: Preview a page in Google search. Even though the preview div will have a ```display: none```, it'll help if put in a an ```aside``` tag so that it can be understood that its a hidden element kind.
    * time
        * Indicates that it shows time.
    * mark
        * Has a GUI effect
        * Highlights the part
        
---
#### link

* ```<link rel="shortcut icon" href="imaganame.ico" type="image/ico" />```  
for favicon
* ```<link rel="alternate" href="http://someurl/" type="application/rss+xml" />```  
can be used to give page in multiple ways! The above example would be a html page and there's an alternate version of the same page in the specified url as an rss.
* The ```rel="sidebar"``` attribute for an anchor does the following -
    * Asks for bookmarking
    * Then when loaded loads on a sidebar.
* The ```rel="pingback"``` can be used for that blogging feature sir said
    