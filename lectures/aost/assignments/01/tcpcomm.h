#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <signal.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <getopt.h>

#define ERROR_SOCKET	1
#define ERROR_BIND		2
#define ERROR_LISTEN	3
#define ERROR_ACCEPT	4
#define ERROR_CLOSE		5
#define ERROR_SETSOCKOPT 6
#define ERROR_RECV		7
#define ERROR_SEND		8