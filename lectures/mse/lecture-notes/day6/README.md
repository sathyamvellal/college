24-08-2013
=========

### Agenda
* Frameworks

---
### Notes
* primitive types have wrapper classes as it can support message passing.

---
### Frameworks
* Foundation Framework - 
    * base
    * geometry related functions
* Application Kit Framework - GUI
    * for OS X
* Cocoa - Above two + Core Data
* Cocoa Touch - Use *UIKit Framework* (mobile related UI) instead of *Application Kit Framework* (desktop related UI)

---
### Foundation Framework
* in **Foundation/Foundation.h**
* NSNumber
* NSString
* NSMutableString\
* NSArray
    * subArrayWithRange:NSMakeRange(i, j) - starts with i and j elements
* NSMutableArray
* NSDictionary
* NSMutableDictionary
* NSSet

---
###  Assignment
  
1. Create a dictionary and add the following key/value pairs to the dictionary.  
Centimeter: 10, Pound: 40, Ounce: 50, Kilogram: 20, Yard: 30, Millimeter: 10, Kilometer: 10, milligram: 20, gram: 20, meter: 10  
(keys are NSString and values are integers).  
Enumerate through the dictionary and print the values of those keys which end with 'meter'
2. NSDate is a data type for working with dates. Using the methods of NSDate class, print the following -
    1. Today's date
    2. Day after tomorrow's date
    3. Last Thursday's date
    4. The earlier date among a given set of dates
    5. The 10th day of the month given the first day