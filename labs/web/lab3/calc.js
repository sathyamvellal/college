self.onmessage = fact;

function fact(event) {
	data = event.data;
	res = 1;
	
	if (data) {
		num = parseInt(data);
		
		while (num > 0) {
			res *= num;
			--num;
		}
	}
	sleep(1);
	
	postMessage (res);
}

function sleep(delay) {
	var now = new Date();
	var desiredTime = new Date().setSeconds(now.getSeconds() + delay);
	
	while (now < desiredTime) {
		now = new Date(); // update the current time
	}
}