Lab 3 - 27-08-2013
==================

Create a phonebook for a user and add records into the phone book.  
Each Record contains the name of the person, email id and his contact number.  
Also perform the following operations on the phonebook.
* Delete the record 
* Search for a record
* Edit the record
