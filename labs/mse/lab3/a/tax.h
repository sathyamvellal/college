#import <Foundation/Foundation.h>
#import <Foundation/NSObject.h>

@protocol Tax<NSObject>
- (double) getTotalPrice:(double)amount;
- (double) getTax:(double)amount;
@end

@interface FinishedTax : NSObject<Tax>
{
	@private double kstRate;
	@private double cstRate;
}
- (double) getTotalPrice:(double)amount;
- (double) getTax:(double)amount;
@end 

@interface GroceryTax : NSObject<Tax>
{
	@private double price;
	@private double vatRate;
}
- (double) getTotalPrice:(double)amount;
- (double) getTax:(double)amount;
@end 
