#include	"tcpcommon.h"
#include	"child.h"

#define	MAX_CHILDREN	100

static int		g_total_children;
Child_info		*g_child_info;
pid_t		child_make(int, int);

/* ------------------------------------------------------ */
void validate(int argc, char **argv, char **ip_addr, short *port, int *cnt_child) {
	int		opt;

	while ((opt = getopt(argc, argv, "i:p:c:")) != -1) {
		switch (opt) {
		case 'i':
			*ip_addr = optarg;
			break;

		case 'p':
			*port = atoi(optarg);
			break;

		case 'c':
			*cnt_child = atoi(optarg);
			break;

		default:
			fprintf(stderr, "Usage: %s -i <ipaddr> -p <port> -c <num_children>\n", argv[0]);
			exit(1);
			break;
		}
	}
	if ((*port == 0) || (*ip_addr == (char *)NULL)) {
		fprintf(stderr, "ipaddr/port num not specified or invalid value\n");
		exit(1);
	}
	return;
}
/* ------------------------------------------------------ */
int main(int argc, char **argv)
{
	int			listenfd; /* socket for listening new connections */
	int			ii; /* index variable */
	int			max_fd; /* max value of socket id */
	fd_set		rset; /* master set of children */
	fd_set		wk_rset; /* working set for current children */
	socklen_t	clilen;
	struct sockaddr_in	cliaddr; /* IP address of new connecting client */
	struct sockaddr_in	servaddr; /* IP address on which server accepts */
	char 		*ip_addr = (char *) NULL; /* IP address on which client connects*/
	short 		port = 0;  /* port number on to which client connects */
	int			cnt_children;	/* count fo pre-forked children */
	int			cnt_avail; /* num of children available to handle new request */
	struct timeval timeout;
	int			cnt_fd;	/* number of fds returned by select */
	int			child_fd;
	int			conn_fd; /* fd of new accepted connection */
	int			cnt_bytes; /* num of bytes read/written */
	char		cbuf;
	int			status; /* status of system calls */

	/* ensure valid command line params are specified */
	validate(argc, argv, &ip_addr, &port, &cnt_children);

	cnt_avail = cnt_children;
	g_child_info = calloc(cnt_children, sizeof(Child_info));

	listenfd = socket (AF_INET, SOCK_STREAM, 0);

	bzero((void *)&servaddr, sizeof(servaddr));
	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = inet_addr(ip_addr);
	servaddr.sin_port = htons(port);

	status = bind(listenfd, (struct sockaddr *) &servaddr, sizeof(servaddr));
	status = listen(listenfd, LISTENQ);

	FD_ZERO(&rset);
	FD_SET(listenfd, &rset);
	max_fd = listenfd;

	/* prefork all the children */
	for (ii = 0; ii < cnt_children; ii++) {
		child_make(ii, listenfd);	/* parent returns */
		child_fd = g_child_info[ii].child_pipefd;
		FD_SET(child_fd,  &rset);
		max_fd = max_fd > child_fd ? max_fd : child_fd;
	}

	for ( ; ; ) {
		wk_rset = rset;
		if (cnt_avail <= 0) {
			FD_CLR(listenfd, &wk_rset);	/* turn off if no available children */
		}
		cnt_fd = select(max_fd + 1, &wk_rset, NULL, NULL, NULL);

			/* 4check for new connections */
		if (FD_ISSET(listenfd, &wk_rset)) {
			clilen = sizeof(cliaddr);
			conn_fd = accept(listenfd, (struct sockaddr *)&cliaddr, &clilen);

			for (ii = 0; ii < cnt_children; ii++) {
				if (g_child_info[ii].child_status == CHILD_FREE) {
					break;	/* child is available */
				}
			}

			if (ii == cnt_children) {
				err_quit("no available children");
			}
			g_child_info[ii].child_status = CHILD_BUSY;	/* mark child as busy */
			g_child_info[ii].child_count++;
			cnt_avail--;

			/* pass the fd descriptor of new accepted connection to free child */
			cnt_bytes = write_fd(g_child_info[ii].child_pipefd, "", 1, conn_fd);
			close(conn_fd);
			if (--cnt_fd == 0)
				continue;	/* all done with select() results */
		}

		/* find any newly-available children */
		for (ii = 0; ii < cnt_children; ii++) {
			if (FD_ISSET(g_child_info[ii].child_pipefd, &wk_rset)) {
				if ( (cnt_bytes = read(g_child_info[ii].child_pipefd, &cbuf, 1)) == 0) {
					err_quit("child %d terminated unexpectedly", ii);
				}
				g_child_info[ii].child_status = CHILD_FREE;
				cnt_avail++;
				if (--cnt_fd == 0)
					break;	/* all done with select() results */
			}
		}
	}
}
