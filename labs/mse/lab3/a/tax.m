#import <Foundation/Foundation.h>
#import "tax.h"

@implementation FinishedTax
- (id) init {
	kstRate = 0.18;
	cstRate = 0.04;
	return self;
}

- (double) getTotalPrice:(double)amount {
	return amount + [self getTax:amount];
}

- (double) getTax:(double)amount {
	double kstPrice = amount * kstRate;
	double cstPrice = (amount + kstPrice) * cstRate;
	return kstPrice + cstPrice;
}
@end

@implementation GroceryTax
- (id) init {
	vatRate = 0.18;
	return self;
}

- (double) getTotalPrice:(double)amount {
	return amount + [self getTax:amount];
}

- (double) getTax:(double)amount {
	return amount * vatRate;
}
@end
