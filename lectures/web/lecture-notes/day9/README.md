03-09-2013
==========

### Agenda

* XML contd
* XMLNS
* XSLT

---
### XML Contd.. XMLNS

* ```xmlns="http://somesite1.com" xmlns:pub="http://somesite2.com"```. URI prefixed with pub. Usually a URL.
* The first xmlns is the default namespace. ```:pub``` gives additional namespace.
In XML we write tag as ```<pub:title>Some Title</pub:title>
* The default namespace in IE is ```na```
* so the xpathstr is now ```xpathstr = "na:book/pub:title"```. To enable using namespaces with the ```selectNodes``` function, do the following
    * ```ns = "xmlns="http://somesite1.com" xmlns:pub="http://somesite2.com"```
    * And ```xmldom.setProperty("SelectNamespaces"ns);```
* For other browsers - 
    * ```xmldom = zXmlDom.createDocument();```
    * ```xmldom.load("file.xml");```
    * ```xpathstr = "book/title"```
    * ```results = xmldom.evaluate(xpathstr, xmldom.documentElement, null, XPathResult.ORDERED_NODE_ITERATOR_TYPE, null);```
        * The string
        * Node on which it has to be evaluated
        * Namespace resolver - a function
        * Result type
            * ```STRING_TYPE```
            * ```BOOLEAN_TYPE```
            * ```NUMBER_TYPE```
            * ```ORDERED_NODE_ITERATOR_TYPE```
            * ```UNORDERED_NODE_ITERATOR_TYPE```
        * Object to populate. Returns the object if null
    * ```while (node = results.iterateNext()) { }```
    * The namespace resolver takes one parameter, the prefix. Write a switch case inside. Example of a case ```case 'na': url="xmlns="http://somesite1.com"; break;``` and etc. For all namespaces that are present.
    * But when using namespaces, ```xpathstr = "na:book/pub:title";```
    * You can also use ```zXPath.selectNodes(node, expression, ns);```. Works for all browsers
    
---
### XSLT

* **X**ML **S**tyesheet **L**anguage **T**ransformation
* Write rules in a separate file
* file extension ```.xsl```
* This tag ```<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"/>```
* ```<xsl:output method="html" omit-xml-declaration="yes" indent="yes"```
    * the method attribute specifies the language to transform to
* Use ```<xsl:template match="/">...</xsl:template>``` to match the root.
* use ```<xsl:template match="book">...</xsl:template>``` to match some node.
* Use ```<xsl:variable name="title" select="title"/>``` 
* Use ```<xsl:value-of select="$name"/>```
* To apply rules in a XML file use ```<?xml-stylesheet type="text/xsl" href="books.xsl"?>
* To apply the transformation on a xml 
```
xmldom.load("file.xml");
xsldom.load("file.xsl");
retstr = xmldom.transformNode(xsldom);
win = window.open("", "mywin");
win.document.writeln(retstr);
win.document.close();
```
* This is way is used because everytime ```transformNode``` is called, IE parses and validates the xsldom. Refer notes for the other.
    * Create XSLTemplate ActiveXObject ```xsltemplate = new AcitveXObject("MSXML2.XSLTemplate.3.0");```
    * Create XSLProcessor ```xslproc = xsltemplate.createProcessor();```
    * Feed input dom ```xslproc.input = xmldom;```
    * Transform ```xslproc.transform(); ```
    * Write it onto document ```win.document.writeln(xslproc.output);```
    * To change input
        * reset ```xslproc.reset();```
        * change input ```xslproc.input = newxmldom;```
        * transform ```xslproc.transform();```