#include <stdio.h>
#include <stdlib.h>

/* program will sum the paramers and return double the value */
int double_sum(int op1, int op2) {
	int sum;

	sum = 2 * op1 + op2;
	return sum;
}

int main(int argc, char *argv[]) {

	int op1 = 11; /* first operand */
	int op2 = 22; /* second operand */
	int	result;

	sleep(1);
	if (argc > 1) {
	  op1 = atoi(argv[1]);
	}
	sleep(1);
	if (argc > 2) { 
	  op2 = atoi(argv[2]);
	}

	sleep(1);
	result = double_sum(op1, op2);
	printf("the result is %d\n", result);
	exit(0);
}
