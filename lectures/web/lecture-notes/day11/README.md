12-09-2013
=========

### Agenda

* Async using `<img`>

---
### Async using `<img>`

#### Possible Approach

Use JS. create `<img>` element. Don't add to DOM. send a small image from server and send text using cookies.

* Cons
   * Cookies can be disabled.
    * `<img>` can be disabled. 
    * Bandwidth. Cookies may be small (ie text). image sent may be quite big when compared to text.
    * Cannot load binary data.
* Pros
    * Simple to code
    * Can do cross domain calls. Not possible using hidden frames (that convention)
    * No compatibility issues. Images are loaded same in all browsers.
    * Error handling possible; unlike hidden frames
        * images fire `onload` and `onerror` events.
        
#### Create Image

* PHP
    * `$img = imagecreate(10, 10);`
    * `imagecolorallocate($img, 140, 180, 100);`
    * `imagejpeg($img);`
    * `imagedestroy($img);`
   
* Uses
    * To run something in the server or do something in the server, checking something, etc with very little info to return back to the client; this method can be used to fire such signals.
    
* Pros
    * GET and POST
    * Code is clean
    * Binary possible
    * streaming is possible
    * Upload files
    
---
### XHR Level 2

* `xhr.responseType` sets the type of response of the xhr request.
* if `responseType` is set, server populates response in `xhr.response`. Otherwise, by default it goes to `responseText`.
* to create URL for object locally, use `url = URL.createObjectURL(variable_name);`.
* Create forms in JS.
    * `form = new FormData();`.
* Get files from a form
    * `getElementById`
    * `elem.files[0];`.
* Add an element to JS Form 
    * `form.append("name", "value");`
* Send that form using xhr
    * `xhr,send(form);`.